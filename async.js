class DoAsync {
  #registerDoneCallback = [];    
  #registerFailCallback = [];

  constructor(fn) {    
    let isCalled = 0;
    const done = (arg) => {
      isCalled++;
      if (isCalled  <= 1) {
      setTimeout(() => this.#registerDoneCallback.forEach(callback => callback(arg)), 0); 
      }
      return null;
    } 
    
    const fail = (arg) => {
      isCalled++;
      if (isCalled  <= 1) {
      setTimeout(() => this.#registerFailCallback.forEach(callback => callback(arg)), 0);
    }
    return null;
    }
    fn(done, fail);
  }

  after (cb) {
    this.#registerDoneCallback.push(cb);
  }

  error (cb) {
    this.#registerFailCallback.push(cb);
  }
}